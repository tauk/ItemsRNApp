import React from 'react';
import { FlatList, ActivityIndicator, Text, View  } from 'react-native';

export default class ViewItemsApp extends React.Component {

  constructor(props){
    super(props);
    this.state = { isLoading: true}
  }

  componentDidMount(){
    this.loadSensors()
    this.loadMicros()
  }

  loadSensors() {
    let request = new XMLHttpRequest();
    request.onreadystatechange = (e) => {
      if (request.readyState !== 4) {
        return;
      }
    
      if (request.status === 200) {
        console.log('success', request.responseText);
        this.setState({
          isLoading : false,
          sensorsData : JSON.parse(request.responseText)
        }, function() {

        });
      } else {
        console.warn('error');
      }
    };
    
    request.open('GET', 'http://10.15.34.211:7979/sensors/');
    request.send();
  }

  loadMicros() {
    let request = new XMLHttpRequest();
    request.onreadystatechange = (e) => {
      if (request.readyState !== 4) {
        return;
      }
    
      if (request.status === 200) {
        console.log('success', request.responseText);
        this.setState({
          isLoading : false,
          microsData : JSON.parse(request.responseText)
        }, function() {

        });
      } else {
        console.warn('error');
      }
    };
    
    request.open('GET', 'http://10.15.34.211:7979/micros/');
    request.send();
  }


  render(){
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View style={{flex: 0.75, paddingTop:20, alignItems:'center'}}>
       
        <Text style={{fontWeight:'bold', paddingTop:5}}>Sensors</Text>
          
        <FlatList style={{flex: 0.75, paddingTop:5}}
            data={this.state.sensors}
            renderItem={({item}) => <Text>{item.item_name}, Price: AED {item.price.toFixed(2).toString()}</Text>}
            keyExtractor={(item, index) => index}
        />

        <Text style={{fontWeight:'bold', paddingTop:5}}>Microcontrollers</Text>
          
          <FlatList style={{flex: 0.75, paddingTop:5}}
              data={this.state.microsData}
              renderItem={({item}) => <Text>{item.item_name}, Price: AED {item.price.toFixed(2).toString()}</Text>}
              keyExtractor={(item, index) => index}
          />
      </View>
    );
  }
}
